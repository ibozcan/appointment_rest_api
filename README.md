# Appointment API

Appointments API is made for estate agents. It provides store contacts and appointments. Also, API calculates time that spent in traffic and distance between estate agent and appointment address.

## Instructions

Firstly clone repository

```bash
git clone https://gitlab.com/ibozcan/appointment_rest_api.git
```

Install php 

```bash
apt install php php-cli php-fpm php-json php-common php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath
```

Install composer

```bash
apt install composer
```
Move to project folder and install packages

```bash
composer install
```

Create env

```bash
cp .env.example .env
```
Generate app key

```bash
php artisan key:generate
```

Now project is up but we should serve it I used nginx, install nginx

```bash
apt install nginx
```

Start nginx service

```bash
service nginx start
```

Check nginx is up
```
netstat -ntlp
```
You should see :80 port in table. If you see, configure nginx config file. Move to config folder

```bash
cd /etc/nginx/sites-avaliable
```

Open default file with 
```bash
nano default
```

Now, change to root path it means public folder in your laravel project. Then add index.php to index file list. Now delete location and add this snippet

```nginx
    location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

    location ~ .php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

```

Now, nginx config is ready. When you open localhost, you should see laravel landing page. Let's install mysql

```bash
apt install mysql-server
```
Mysql server installed you can check it with

```bash
service mysql status
```

If status is active create database. Open a mysql bash

```bash
mysql
```

Now create password and alter user

```mysql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Current-Root-Password';
FLUSH PRIVILEGES;
```

Now create a database
```mysql
CREATE SCHEMA appointment_db;
```

Now move to project folder and edit env for mysql. Edit DB_* parameters for your user and connection.

Then migrate tables

```bash
php artisan migrate
```

Now project is totally up. Now for mapquest api add your api key to bottom of the env as

MAPQUEST_API_KEY=your_key

You can get your free key from [here](https://developer.mapquest.com/)


## Documentation

I already create a documentation for endpoints.

https://documenter.getpostman.com/view/11927878/U16bxpdT





