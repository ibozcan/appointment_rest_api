<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


use Symfony\Component\HttpFoundation\Response;



class AuthController extends Controller
{

    public function login_page(Request $request){

        if(!$request->cookie('jwt')){
            return response()->json([
                'code' => 401,
                'message' => 'Unauthhorized',
                'data' => []
            ]);
        }

        return response()->json([
            'code' => 304,
            'message' => 'Redirected',
            'data' => []
        ]);

       
    }

    public function register(Request $request){

        $rules=array(
            'email' => 'required|email',
        );


        $validator=Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json([
                'code' => 404,
                'message' => 'Invalid mail type',
                'data' => []
            ]);
        }

        $user = User::firstOrcreate([
            'email' => $request->input('email'),
        ],[
            'name' => $request->input('name'),
            'password' => Hash::make($request->input('password'))
        ]);


        if(!$user){
            return response()->json([
                'code' => 400,
                'message' => 'User is not created',
                'data' => []
            ]);
        }
        
        if(!$user->wasRecentlyCreated){
            return response()->json([
                'code' => 201,
                'message' => 'Already has a account',
                'data' => $user
            ]);
        }

        return response()->json([
            'code' => 201,
            'message' => 'User created',
            'data' => $user
        ]);
    }

    public function login(Request $request){

        $rules=array(
            'email' => 'required|email',
        );


        $validator=Validator::make($request->all(),$rules);

        if(!Auth::attempt($request->only('email','password')) || $validator->fails() ){
            return response()->json([
                'code' => 404,
                'message' => 'Invalid credentials',
                'data' => []
            ]);
        }
        
        $user = Auth::user();
        $token = $user->createToken('token')->plainTextToken;
        $cookie = cookie('jwt',$token,20);

        
        return response()->json([
            'code' => 201,
            'message' => 'User logged in',
            'data' => $user
        ])->withCookie($cookie);
    }

    public function logout(Request $request){
        $cookie = Cookie::forget('jwt');
        return response()->json([
                'code' => 201,
                'message' => 'User logged out',
                'data' => []
            ])->withCookie($cookie);
    }
}
