<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Contact;
use App\Models\Appointment;
use Illuminate\Support\Facades\Http;

use App\Http\Helpers\DistanceCalculator;




class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation rules
        $rules=array(
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'address' => 'required'
        );
        //Validation messages
        $messages=array(
                'name.required' => 'Please enter a name.',
                'surname.required' => 'Please enter a surname.',
                'email.required' => 'Please enter a email.',
                'address.required' => 'Please enter a address.',
                'date.required' => 'Please enter a date.'
        );


        $validator=Validator::make($request->all(),$rules,$messages);

        if($validator->fails())
        {
            return response()->json([
                'code' => 400,
                'message' => $validator->messages(),
                'data' => []
            ]);
        }

        $contact = Contact::firstOrCreate([
            'email' => $request->input('email'),
        ],[

            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'phone' => $request->input('phone')

        ]);


        if(!$contact){ //If contact not saved
            return response()->json([
                'code' => 400,
                'message' => 'Contact is not saved',
                'data' => []
            ]);
        }

        // 1. param origin, 2. destination, 3. date of appointment
        $distance_calculator = new DistanceCalculator('cm27pj',$request->input('address'),$request->input('date'));
        $check_in_out_dates = $distance_calculator->check_in_out(); // returns checkout_date, checkin_date and distance
        $checkout_date = $check_in_out_dates['checkout_date'];
        $checkin_date = $check_in_out_dates['checkin_date'];
        $distance = $check_in_out_dates['distance'];

        if(is_numeric($checkin_date) && is_numeric($checkout_date) && is_numeric($distance)){ // Address is invalid
            return response()->json([
                'code' => 404,
                'message' => 'Address is not in England',
                'data' => []
            ]);
        }

        $appointment = Appointment::create([
            'contact_id' => $contact->id,
            "address" => $request->input('address'),
            "checkout_time" => $checkout_date,
            "checkin_time" => $checkin_date,
            "date" => $request->input('date'),
            'distance' => $distance
        ]);

        if(!$appointment){
            return response()->json([
                'code' => 400,
                'message' => 'Appointment is  not created',
                'data' => []
            ]);
        }

        return response()->json([
            'code' => 201,
            'message' => 'Appointment created',
            'data' => $appointment
        ]);

         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $appointment = Appointment::find($id);

        if(!$appointment){
            return response()->json([
                'code' => 404,
                'message' => 'Appointment is not found',
                'data' => []
            ]);
        }

        $contact = Contact::firstOrCreate([
            'email' => $request->input('email'),
        ],[

            'name' => $request->input('name'),
            'surname' => $request->input('surname')
        ]);


        if($request->input('date') !== $appointment->date){ // if date is changed
            $distance_calculator = new DistanceCalculator('cm27pj',$request->input('address'),$request->input('date'));
            $check_in_out_dates = $distance_calculator->check_in_out(); // returns checkout_date, checkin_date and distance
            $checkout_date = $check_in_out_dates['checkout_date'];
            $checkin_date = $check_in_out_dates['checkin_date'];
            $distance = $check_in_out_dates['distance'];

            // update operation
            $appointment->date = $request->input('date');
            $appointment->checkout_time = $checkout_date;
            $appointment->checkin_time = $checkin_date;
            $appointment->distance = $distance;
            $appointment->contact_id = $contact->id;

            if(!$appointment->save()){
                return response()->json([
                    'code' => 400,
                    'message' => 'Appointment '.$appointment->id.' not updated',
                    'data' => []
                ]);
            }

            return response()->json([
                'code' => 201,
                'message' => 'Appointment is updated',
                'data' => ['appointment' => $appointment, 'contact' => $contact]
            ]);

        }

        return response()->json([
            'code' => 202,
            'message' => 'Dates are same',
            'data' => ['appointment' => $appointment, 'contact' => $contact]
        ]);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = Appointment::find($id);
        if(!$appointment){
            return response()->json([
                'code' => 404,
                'message' => 'Appointment is not found',
                'data' => []
            ]);
        }

        if(!$appointment->delete()){
            
            return response()->json([
                'code' => 400,
                'message' => 'Appointment '.$appointment->id.' is not deleted',
                'data' => []
            ]);
        }

        return response()->json([
            'code' => 201,
            'message' => 'Appointment deleted',
            'data' => $appointment
        ]);



    }
}
