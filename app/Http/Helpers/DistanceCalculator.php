<?php
namespace App\Http\Helpers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;



class DistanceCalculator{

    private $origin;
    private $destination;

    public function __construct($origin,$destination,$appointment_date) {
        $this->origin = $origin;
        $this->destination = $destination;
        $this->appointment_date = $appointment_date;
    }



    public function calculate(){

        $destination_response = Http::get('http://api.postcodes.io/postcodes/'.$this->destination)->object();

        if($destination_response->status == 404 || $destination_response->result->country != 'England' ){
            return ['distance'=> 0, 'time' => 0];
        }

        $destination_lat = $destination_response->result->latitude;
        $destination_long = $destination_response->result->longitude;
        $origin_response = Http::get('http://api.postcodes.io/postcodes/'.$this->origin)->object();
        $origin_lat = $origin_response->result->latitude;
        $origin_long = $origin_response->result->longitude;

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->post('http://www.mapquestapi.com/directions/v2/routematrix?key='.env('MAPQUEST_API_KEY'), [
            "locations" => [
                $origin_lat.','.$origin_long,
                $destination_lat.','.$destination_long,
            ],
            "options" => [
                "allToAll" => false,
                "unit" => "k"
            ]
        ])->object();


        return ['distance' => $response->distance[1], 'time'=>$response->time[1]];
    }

    public function check_in_out(){

        $distance_infos = $this->calculate();
        $interval = $distance_infos['time'];
        $distance = $distance_infos['distance'];

        if($interval == 0 && $distance == 0){ // if address is invalid
            return ['checkin_date' => 0, 'checkout_date' => 0, 'distance' => 0];
        }

        $appoinment_date = Carbon::createFromFormat('Y-m-d H:i:s',  $this->appointment_date);
        $checkout_date = $appoinment_date->subMinutes($interval/60);
        $appoinment_date = Carbon::createFromFormat('Y-m-d H:i:s',  $this->appointment_date);
        $checkin_date = $appoinment_date->addMinutes($interval/60 + 60);

        return ['checkin_date' => $checkin_date, 'checkout_date' => $checkout_date, 'distance' => $distance];

    }



    

}